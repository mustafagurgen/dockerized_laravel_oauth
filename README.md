## About Project
Daily weathercast reports will be saved to database via cron jobs.
Daily notifications will be sent to users via cron jobs.   
The users can see the daily weathercast of the location they have chosen. 
The users will be limited to view weathercast for each location and can increase viewing counts via buying coupons. 

## Run the command below to installcomposer and create the laravel project
docker run --rm -v $(pwd):/app composer install
docker run --rm -v $(pwd):/app composer create-project --prefer-dist laravel/laravel myapp

## Excluded Folders
  # apitest 
    - the folder that include sample files of http requests for the rest api services.
  # crons
    - the folder that includes php and bash(.sh) script files that run as crontab jobs.
  # mysql
    - the folder that includes mounted mysql-server data and config files
  # php
    - the folder that mounted the php-fpm.ini file
  # nginx
    - the folder that mounted web server(nginx) config file

## Introduction of nginx, mysql, php-fpm and composer as service using docker compose
  # Configuring docker-compose.yml for services
  - Configure nginx service in nginx folder
  - Configure mysql-server data folder and my.cnf settings in mysql folder
  - Configure php.ini file in php folder 
  - run the project with command docker-compose up -d

## Mysql Queries
  - ORM Related Database Query type is used for listing data.
  - Elequent Query type is used only in ORM Query for the tables fields(Example: Order::select(DB::raw('sum(price) as total'))...). 

## Modal 
  # app/Modal
    -- all modal files are created under app/Modal folder(Exclude default created User modal file) 
    
    docker-compose exec app php artisan make:model Model/Location -m (-m used for migration file)

## Controller 
  # app/Http/Controllers/Api
    -- Rest Api Controller files are created in app/Http/Controllers/Api 
    docker-compose exec app php artisan make:Controller Api/UserCouponController

## Crons
  You can set cron jobs adding lines below to crontab file via crontab -e command.
  0 9 * * * /usr/bin/php ./crons/cron.php #php dosyasının tam yolu yazılmalı 

## Oauth 
  - laravel/passport package is used for oauth2 authentcation.
  - token expire time is set as 15 minutes. If you want to make differences in time you can modify;
    "public function boot()" method in AuthServiceProvider.php file under ./app/Providers/ folder.

  - you can install laravel/passport via command below
    docker-compose exec app php artisan passport:install


## Request and Response Standardization
  ## Traits
  # app/Traits/RequestTrait.php 
    - You can use getPrm() method for request parameters in the project. 
    - Response: used as 3 different methods in the project;
      - resultOk: Returns when process is success.
      - resultError: Returns when process is fail.
      - resultWarning: Returns when process is success with warning.

## Database 
  # Migrations
    - database klasöründeki migration dosyaları ile tablolar oluşturulabilir.
    docker-compose exec app php artisan migrate

## Git Commands
  # Git on existing directory
  git add .
  git remote add origin https://mustafagurgen@bitbucket.org/mustafagurgen/challenge.git
  git pull origin master


  # .gitignore
    - Added to gitignore file folders and files in per line which may cause security risk or big size like .env file or storage folder.


  # first commit
  git add .
  git commit -m "Creating project and setting .env files"
  git push origin master


  # Merge
  git checkout master
  git merge new-modules
  git branch -d new-modules
