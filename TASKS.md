# Hediye Kodu ile Hava Durumu Abonelik Sistemi
  + Abonelerine günlük hava durumu bilgilerinin gönderildiği bir servis geliştirilmeli.
  + Kullanıcıların bu servise REST API aracılığı kaydedilip günlük hava durumu bilgilerini alabilmeli.
  + Bu servise kayıt olan kullanıcıların ücretli abone kabul edilecek, sistemde tanımlı hediye kodları ile kullanıcılar ücretsiz aboneliğe geçebilmeli.
  + Servisin her gün her kullanıcının yerel saatine göre saat 09:00’da o şehirle ilgili hava durumu bilgisini bildirim olarak gönderebilmeli.

# Rest Api
  + Kullanıcı kaydı yapılabilmeli (registration)
  + Kayıtlı kullanıcı login olup auth token alabilmeli, 
    + tokenlar 15 dk süreli olmalı (login)
  + Kayıtlı kullanıcı hediye kodunu aktive edebilmeli (auth token gerektirir) (redeem)
  + Kayıtlı kullanıcı bilgilerini güncelleyebilir (auth token gerektirir) (update)
    # change pass ile password güncellemesi, başka işlemler de eklenebilir

# Beklenenler
  + Rest api request ve response yapılarının tasarlanması
  + Gereken veritabanı yapılarının tasarlanması ve sql kodlarının projeye eklenmesi ( MySQL uyumlu )
    # Migration ile yapıldı.
  + OO ve MVC kod standartlarında geliştirme yapılması
  + API errorlarının standart yapıda olması, kodda tüm hataların yönetilmesi
    # try catch ile olası hataların yakalanması ve response edilmesi sağlandı
  + PSR2 uyumluluğu
  + Açık kaynak kütüphanelerin eklenmesi ve kullanımı
    # laravel/passport kullanıldı
  + Genel README.md dosyası
  + Crona eklenecek satır örneği


## Locations +
  # Fields
    - id (int auto increment)
    - country (int unsigned) -- daha sonra gerekirse ulkeler tablosundan alınacak
    - city (varchar 155 not null)
    - latitude (varchar 100 not null)
    - longitude (varchar 100 not null)
    - postal_code (int unsigned) default 0
    - specs (int unsigned) -- aktif, pasif, vb farklı durumlar için kullanılacak

## Weather Cast +
  # Fields
    - id (int auto increment)
    - loc_id (int unsigned not null) 
    - wc_date (date yyyy-mm-dd)
    - wc_status (varchar 80 not null)
    - wc_day (varchar 20)
    - wc_night (varchar 20)
    - wc_icon (varchar 20)

## User Notification +
  # Fields
    - id (int auto increment)
    - loc_id (int unsigned not null)
    - user_id (int unsigned not null)
    - specs (int unsigned not null) -- 0:pending, 1:notified


## Coupons +
  # Fields
    - id (int auto increment)
    - coupon_code (varchar 15 unique)
    - desc (varchar 255)
    - specs (int unsigned) -- aktif, pasif, kullanıldı, vb farklı durumlar için kullanılacak
    - used_at (datetime yyyy-mm-dd H:i:s)
    
## User Coupons +
  # Fields
    - id (int auto increment)
    - coupon_id (int unsigned not null) 
    - user_id (int unsigned not null)
    
    

