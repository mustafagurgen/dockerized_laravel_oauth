<?php

// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return ['aa'=>'bb']; // $request->user();
// });

Route::prefix('v1')->group(function(){

    // Signed user routes
    Route::group(['middleware' => 'auth:api'], function(){
        Route::post('profile', 'Api\AuthController@getUser');
        Route::post('changepass', 'Api\AuthController@changePassword');

        ## Locations
        Route::post('loc/add', 'Api\LocationController@addLocation');
        Route::post('loc/list', 'Api\LocationController@listLocation');
        Route::post('loc/get', 'Api\LocationController@getLocation');

        ## Weather Cast
        Route::post('weather/list', 'Api\WeatherCastController@listWeather');

        ## User Notification
        Route::post('notification/add', 'Api\UserNotificationController@addNotification');
        Route::post('notification/list', 'Api\UserNotificationController@listNotifications');

        ## Coupons
        Route::post('coupon/add', 'Api\CouponController@addCoupon');
        Route::post('coupon/list', 'Api\CouponController@listCoupons');
        Route::post('coupon/get', 'Api\CouponController@getCoupon');
        
        ## User Coupons
        Route::post('usercoupon/list', 'Api\UserCouponController@listCoupons');
        Route::post('usercoupon/get', 'Api\UserCouponController@getCoupon');

    });

    ## User routes
    Route::post('login', 'Api\AuthController@login');
    Route::post('register', 'Api\AuthController@register');
    // Route::post('forgetpass', 'Api\AuthController@forgetpass');
    
    ## Weather Cast
    Route::post('weather/add', 'Api\WeatherCastController@addWeather');
    Route::post('weather/get', 'Api\WeatherCastController@getWeather');

    Route::post('notification/get', 'Api\UserNotificationController@getNotification');

    ## User Coupons
    Route::post('usercoupon/set', 'Api\UserCouponController@setCoupon');

    ## Cronjobs
    Route::post('crons/emailing', 'Api\CronJobController@getNotifications');


});
