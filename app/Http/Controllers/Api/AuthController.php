<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Traits\RequestTrait;
use Validator;
use App\User;
use Exception;
Use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    // This trait is used for get request parameters and return response(on success or on error)
    use RequestTrait;

    public function register() {
        try {
            $request = $this->getPrm();
            $validator = Validator::make($request, 
            [ 
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'timezone' => 'required|string',
            'lang' => 'required|string',
            'password' => 'required',  
            'c_password' => 'required|same:password', 
            ]);
    
            if ($validator->fails()) {
                $err = $validator->errors();
                if ($err->first('name'))  return $this->resultError("Please enter your name!", null);
                if ($err->first('timezone'))  return $this->resultError("Please select default timezone!", null);
                if ($err->first('lang'))  return $this->resultError("Please select default language!", null);
                if ($err->first('password'))  return $this->resultError("Please insert a valid password!", null);
            }

            $request['password'] = bcrypt($request['password']);
            $user = User::create($request); 
            $success['token'] =  $user->createToken('AppName')->accessToken;
            $success['refreshToken'] =  $user->createToken('AppName')->refreshToken;
            return $this->resultOk('You have successfuly registered',$success);
        }
        catch (Exception $e) {
            return $this->resultError("An error occured while registering!", null);
        }
    }

    public function login(){
        $request = $this->getPrm();
        if(Auth::attempt(['email' => $request['email'], 'password' => $request['password']])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('AppName')-> accessToken; 
            return $this->resultOk("Signed successfuly", $success);
        }
        else { 
           return $this->resultError('Unauthorised',null); 
        }
    }

    private function check_old_pass($pr,$user) {
        if ($pr->welcome == 1) { return true; }
        if (!\Illuminate\Support\Facades\Hash::check($pr->old_password, $user->password)) {
            //return return_result(0, 'The specified password does not match the current password'); }
            return false;
        }
        return true;
    }

    private function updatePassword ($user,$newpass) {
        try {
            // Update neww password
            DB::table("users")->where('id',$user->id)->update(['password'=>$newpass]);
            // Revoke token after password update(Sign again)
            Auth::guard()->user()->token()->revoke();
            return $this->resultOk("Your changes has been saved", null);
        }
        catch (Exception $e) {
            return $this->resultError('An error occured while updating password!', null);
        }
    }

    public function changePassword(){
        $request = $this->getPrm();
        $user = Auth::user();

        if ($user){
            // Use validator to get correct data from request
            $validator = Validator::make($request, 
            [ 
            'currentpass' => 'required',
            'password' => 'required',  
            'passconfirm' => 'required|same:password', 
            ]);
            
            if ($validator->fails()) {
                $err = $validator->errors();
                if ($err->first('currentpass'))  return response()->json(['error'=>"Please enter current password!"], 401);
                if ($err->first('password'))  return response()->json(['error'=>"Please enter the new password!"], 401);
                if ($err->first('passconfirm'))  return response()->json(['error'=>"The new password is not confirmed!"], 401);
            }

            // Check the current password is verified or not
            $current = Hash::check($request['currentpass'],$user->password);
            if (!$current) return response()->json(['error'=>"Current password is not verified!"], 401);

            // If everything is ok then change the password
            return $this->updatePassword($user,bcrypt($request['password']));
        } else{ 
           return $this->resultError('Unauthorised', null);
        } 
    }
    
    public function getUser() {
        try {
            $user = Auth::user();
            return $this->resultOk("User Profile", $user);
        }
        catch (Exception $e) {
            return $this->resultError("An error occurred while retrieving data about your profile!", $e->getMessage()); 
        }
    }

}
