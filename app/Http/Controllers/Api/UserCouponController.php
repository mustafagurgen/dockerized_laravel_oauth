<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\UserCoupon;
use App\Model\Coupon;
use App\User; 
use App\Traits\RequestTrait;
use Carbon\Carbon;
use Validator;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserCouponController extends Controller
{
    use RequestTrait; // Request params function, response function (success or fail)

    private function changeUserAccountType($user_id) {
        try{
            $user = User::select()->where('id',$user_id)->first();
            $user->user_type = 2;
            $user->save();
            return true;
        }
        catch (Exception $e) {
            return false;
        }
    }

    private function updateCouponSpecs($coupon_id) {
        try{
            $cp = Coupon::select()->where('id',$coupon_id)->first();
            $cp->specs = 1; // used
            $cp->used_at = Carbon::now(); // used
            $cp->save();
            return true;
        }
        catch (Exception $e) {
            return false;
        }
    }

    private function updateRelatedData($coupon_id,$user_id) {
        try{
            $cp = $this->updateCouponSpecs($coupon_id);
            $ua = $this->changeUserAccountType($user_id);

            if ($cp && $ua) ['status' => -1, "cp" => $cp, "ua" =>$ua];
            
            return ['status' => 1, "cp" => $cp, "ua" =>$ua];
        }
        catch (Exception $e) {
            return ['status' => -1];
        }
    }

    public function setCoupon() {
        try {

            $request = $this->getPrm();
            $validator = Validator::make($request, 
            [ 
            'coupon_code' => 'required|string',
            'user_id' => 'required|integer|exists:users,id',
            ]);
    
            if ($validator->fails()) {
                $err = $validator->errors();
                if ($err->first('coupon_code'))  return $this->resultError("Please select a valid coupon!", $err);
                if ($err->first('user_id'))  return $this->resultError("There is no related user!", $err);
            }
            $cpn = Coupon::select(DB::raw('id,specs'))->where('coupon_code',$request['coupon_code'])->where('specs',0)->first();
            
            ## Eğer kayıt yoksa işlem yapılmayacak ve hata mesajo gönderilecek
            if (!$cpn) {
                return $this->resultError("This coupon code is not exists or expired!");
            }

            $ucp = new UserCoupon();
            $ucp->coupon_id = (int) $cpn->id;
            $ucp->user_id = (int) $request['user_id'];
            $ucp->save();

            $updates = $this->updateRelatedData($request['user_id'],(int) $cpn->id);

            // Eğer kullanıcı güncellemesi başarısız ise sonuç warning şeklinde dönecek
            if ($updates['status'] === -1 )  {
                return $this->resultWarning('Coupon linked to user with some warning messages!',$updates);
            }
            
            return $this->resultOk('You have successfully link coupon to user',$ucp);
        }
        catch (Exception $e) {
            return $this->resultError("An error occured!", $e->getMessage());
        }
    }

    public function listCoupons() {
        try {
            $user = Auth::user();
            if ($user){
                $request = $this->getPrm();
                $validator = Validator::make($request, 
                [ 
                'start' => 'integer'
                ]);
        
                if ($validator->fails()) {
                    $err = $validator->errors();
                    if ($err->first('start'))  return $this->resultError("The value you have entered must be integer!", null);
                }

                $nt =  UserCoupons::select(DB::raw('id,coupon_id,user_id'))->with('coupons')->where('user_id',$user->id)->get();

                return $this->resultOk('List coupons for user',$nt);
            }
            else {
                return $this->resultError('Unauthorised', null);
            }
        }
        catch (Exception $e) {
            return $this->resultError("An error occured while listing locations!", $e->getMessage());
        }
    }

    public function getCoupon() {
        try {
            $request = $this->getPrm();
            $validator = Validator::make($request, 
            [ 
                'user_id' => 'required|integer',
            ]);

            if ($validator->fails()) {
                $err = $validator->errors();
                if ($err->first('user_id'))  return $this->resultError("You must enter a valid integer value!", null);
                if ($err->first('wc_date'))  return $this->resultError("You must enter a valid date!", null);
            }

            $cpn =  Coupon::select(DB::raw('*'))->with('user')->where('user_id',$request['user_id'])->first();
            return $this->resultOk('List waether cast for given location',$cpn);
        }
        catch (Exception $e) {
            return $this->resultError("An error occured while listing locations!", $e->getMessage());
        }
    }
}
