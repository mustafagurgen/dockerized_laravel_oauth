<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Traits\RequestTrait;
use App\Model\Location;
use Validator;
use Exception;
use Illuminate\Support\Facades\Auth;

class LocationController extends Controller
{
    // This trait is used for get request parameters and return response(on success or on error)
    use RequestTrait;

    public function addLocation() {
        try {
            $user = Auth::user();
            if ($user){

                $request = $this->getPrm();
                $validator = Validator::make($request, 
                [ 
                'city' => 'required|string|max:155',
                'latitude' => 'string|max:80',
                'longitude' => 'string|max:80',
                ]);
        
                if ($validator->fails()) {
                    $err = $validator->errors();
                    if ($err->first('city'))  return $this->resultError("Please enter city name!", null);
                    if ($err->first('latitude'))  return $this->resultError("Latitude length must lower than 80!", null);
                    if ($err->first('longitude'))  return $this->resultError("Longitude length must lower than 80!", null);
                }

                $loc = new Location();
                $loc->city = $request['city'];
                $loc->latitude = $request['latitude'];
                $loc->longitude = $request['longitude'];
                $loc->country = (int) $request['country'];
                $loc->specs = (int) $request['country'];
                $loc->postal_code = $request['postal_code'];
                $loc->save();

                return $this->resultOk('You have successfuly registered',$loc);
            }
            else {
                return $this->resultError('Unauthorised', null);
            }
        }
        catch (Exception $e) {
            return $this->resultError("An error occured while registering!", null);
        }
    }

    public function listLocation() {
        try {
            $user = Auth::user();
            if ($user){

                $request = $this->getPrm();
                $validator = Validator::make($request, 
                [ 
                'start' => 'integer'
                ]);
        
                if ($validator->fails()) {
                    $err = $validator->errors();
                    if ($err->first('start'))  return $this->resultError("You must enter an integer value!", null);
                }

                $loc =  Location::select('*')->limit(50)->offset($request['start'])->get();

                return $this->resultOk('List locations',$loc);
            }
            else {
                return $this->resultError('Unauthorised', null);
            }
        }
        catch (Exception $e) {
            return $this->resultError("An error occured while listing locations!", null);
        }
    }

    public function getLocation() {
        try {
            $user = Auth::user();
            if ($user){

                $request = $this->getPrm();
                $validator = Validator::make($request, 
                [ 
                'slug' => 'required|string|max:155'
                ]);
        
                if ($validator->fails()) {
                    $err = $validator->errors();
                    if ($err->first('slug'))  return $this->resultError("You must enter a correct value!", $err);
                }

                $loc =  Location::select('*')->where('slug',$request['slug'])->first();

                return $this->resultOk('Get location',$loc);
            }
            else {
                return $this->resultError('Unauthorised', null);
            }
        }
        catch (Exception $e) {
            return $this->resultError("An error occured while getting location!", null);
        }
    }

}
