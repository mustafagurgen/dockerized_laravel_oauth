<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserCoupon extends Model
{
    public function coupons() { 
        return $this->hasMany("App\Model\Coupon", "coupon_id")->select(DB::raw('id,coupon_code,`desc`,specs'))->orderBy('id','DESC')->limit(100);
    }

    public function coupon() { 
        return $this->hasOne("App\Model\Coupon", "coupon_id")->select(DB::raw('id,coupon_code,`desc`,specs'))->orderBy('id','DESC');

    }
}
