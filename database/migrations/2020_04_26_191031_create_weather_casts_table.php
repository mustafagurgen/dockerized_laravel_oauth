<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateWeatherCastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weather_casts', function (Blueprint $table) {
            $table->id();
            $table->integer('loc_id')->unsigned()->default(0);
            $table->date('wc_date')->nullable();
            $table->string('wc_status',80)->nullable();
            $table->string('wc_day',20)->nullable();
            $table->string('wc_night',20)->nullable();
            $table->string('wc_icon',20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weather_casts');
    }
}
